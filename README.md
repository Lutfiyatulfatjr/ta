## Video Streaming with Flask Example

### Website
http://www.chioka.in

### Description
Modified to support streaming out with webcams, and not just raw JPEGs.

### Credits
Most of the code credits to Miguel Grinberg, except that I made a small tweak. Thanks!
http://blog.miguelgrinberg.com/post/video-streaming-with-flask

### Usage
1. Install Python dependencies: cv2, flask. (wish that pip install works like a charm)
2. Run "python main.py".
3. Navigate the browser to the local webpage.
=======
# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
from imutils.video import VideoStream
from imutils import face_utils
import datetime
import argparse
import imutils
import time
import dlib
import cv2

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')

class VideoCamera(object):
    def __init__(self):
        # Using OpenCV to capture from device 0. If you have trouble capturing
        # from a webcam, comment the line below out and use a video file
        # instead.
        self.video = cv2.VideoCapture(0)
        # If you decide to use video.mp4, you must have this file in the folder
        # as the main.py.
        # self.video = cv2.VideoCapture('video.mp4')
    
    def __del__(self):
        self.video.release()
    
    def get_frame(self):
        success, image = self.video.read()
        if success == True:
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

            #detect face in grayscale frame
            rects = detector(gray,0)
            
            #loop over face detection
            for rect in rects:
                # determine the facial landmarks for the face region, then
		        # convert the facial landmark (x, y)-coordinates to a NumPy
		        # array
                shape = predictor(gray, rect)
                shape = face_utils.shape_to_np(shape)

                #loop over the (x,y)-coordinates for the facial landmarks
                #and draw them on the image
                for(x,y) in shape:
                    cv2.circle(image, (x,y), 1, (0,0,255), -1)
            
            
        # We are using Motion JPEG, but OpenCV defaults to capture raw images,
        # so we must encode it into JPEG in order to correctly display the
        # video stream.
        ret, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()